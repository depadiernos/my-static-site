/* eslint-disable react/no-unescaped-entities */
import React from 'react'
//

export default () => (
  <div style={{ maxWidth: '800px', paddingBottom: '50px' }}>
    <h1>This is my Netlify Homework. ;)</h1>
    <p>The following are my thoughts and answers (including the questions they relate to).</p>
    <div>
      <h3>Rank your 5 favorite, and least favorite, activities from this list: <a href="https://gist.github.com/fool/b0f254ff8c72a5765b6a9138249789d6">https://gist.github.com/fool/b0f254ff8c72a5765b6a9138249789d6</a></h3>
      <p><strong>Favorites:</strong>
        <li>Debug a customer's build using a programming language and framework that you've never seen before</li>
        <li>Submit bug reports and potentially bug fixes to closed and open source projects that Netlify maintains on GitHub</li>
        <li>Set up your own copy of several static site frameworks for debugging</li>
        <li>Suggest and champion improvements to the product and workflow to your colleagues in and out of support</li>
        <li>Work with people to figure out if Netlify's service can solve a particular workflow or integration challenge they have</li>
      </p>

      <p><strong>Not so favorites:</strong>
        <li>Deliver a talk to many people you don't know at a conference or meetup</li>
        <li>Respond to Netlify fans on Twitter</li>
        <li>Dig through server logs to troubleshoot a customer's website behavior</li>
        <li>Work with prospective customers to explain our service and the pricing model</li>
        <li>Engage multiple users at once via chat to answer their questions and troubleshoot problems</li>
      </p>

      <h3>What is your favorite thing about providing technical support?</h3>
      <p>My favorite thing about providing technical support is <code>“making someone’s day”</code> after a particularly difficult case. A memorable moment of this type was when a customer came to chat with Support at my current job and asked to let me know that I was <code>“the cat’s pajamas”</code>. Not sure what that means, but it sounds awesome. The customer started out irrate due to a drawn-out issue, which I was able to resolve with some intense log-reading. I just made sure I was transparent with the information I had, fully listened to the customer, and gave a clear resolution. That made all the difference.</p>

      <h3>What did you think of our service during the time you used it?  Be honest!  “it sucked” isn’t a wrong answer unless you don’t elaborate and provide some constructive criticism ;)</h3>
      <p>It’s the bee’s knees! Having some experience with Jenkins for deploys and setting up a few <code>LA(e)MP</code> stacks in the past, I can say that Netlify has simplified my life a lot. I like the auto-deploy feature, where a deploy has never been as easy as merging a PR to master. Gitlab does have a similar feature but needs a YAML file to configure. Because of that, it pales in comparison to what Netlify offers.</p>
      <p>I can gush over what Netlify has to offer all day, but I did want to provide some constructive <code>‘criticism’</code> as well. The project settings page, while nicely organized, has some low-contrast areas and minimal visual cues. Though it only took a few moments to get used to it. Also, I found the separation of <code>‘Support’</code> and <code>‘Docs’</code> took a little time to get used to as I would always click <code>‘Support’</code> to look for documentation, which is <code>‘self-serve support’</code> in my mind. I do see there is a link to <code>‘Docs’</code> on that page but perhaps integrating these two support-related pages would make it less likely a customer would click the wrong link.</p>

      <h3>Tell about how you made your site and why you chose the tools you did.  Briefly explain a challenge you experienced in setting up this site and how you solved it.</h3>
      <p>I chose https://www.staticgen.com/react-static. It’s fairly new on the scene, but I really like the simplicity of the framework. I initially wanted to go with Gatsby, which a far more mature solution. I changed my mind when I saw how new react-static was. I love trying new tech. So I nuked my Gatsby instance and initialized a react-static app, instead. Pretty straightforward. I’ve started reactjs projects from scratch before so using a static site generator wasn’t too foreign to me.</p>
      <p>I got the gitlab repo linked to my Netlify project and off it goes. After a minute or so, the site was deployed. I did notice that the pre-rendered html was not picking up the css from the styled-components inline styles. I’ve heard this referred to as a FOUC (flash of unstyled components), which should probably be an abbreviation rather than an acronym to avoid sounding inappropriate. :P So I searched the issues page of the github repo for react-static and sure enough, there was a bug reported with styled-components not working. After updating to the ‘fresh of the press’ version 3.0.1, I encountered a lot of angry dependency issues when building locally. It mentioned ‘react-router-dom’ and ‘react-hot-reload’. My first gut reaction was to ‘npm i’ those dependencies. I continued to fiddle with the config files and got the errors to change to a reference to an 'undefined'. After a bit of reading, I found a <a href="https://github.com/nozzle/react-static/issues/91">bug report</a> confirming that what I had run into was indeed a bug and made a quick comment to confirm that I too had the issue. After a few hours, <a href="https://github.com/tannerlinsley">@tannerlinsley</a> made a commit that fixed the issue. Yay! After updating my project, things were working nicely. And … sanity has returned. Yay again!</p>

      <h3>Could you give us a suggestion to improve this test or the job posting?</h3>
      <p>I actually really enjoyed reading the job posting. But, putting on my constructive criticism glasses, I would probably suggest tweaking the headings to: <code>“Overview”, “About Netlify”, “About You”, “Skills You’ll Need”</code> and <code>“About The Team”</code>. This give a better overview of each section and what to expect to read. It’s a small niggle though.</p>
      <p>With regards to this test, it’s hard to say right now how it could be improved. I’m not yet completely familiar with what and how the workflow is. But right now, I think perhaps making the redirect a requirement and create a different bonus question. This would give the applicant more opportunities to read through the Docs. But again, I’m pretty enthused by the whole process so far. Kudos to Netlify!</p>

      <h3>Provide a link to documentation for a technical/developer-focused product, that you think are well done, and briefly explain why you think they are well done.</h3>
      <p>I think the documentation for <a href="https://reactjs.org/">https://reactjs.org</a> is very well done. Pretty much all the subjects have interactive information, where you are provided examples and are able to ‘play’ with the code on codepen. And the information is organized from a simple intro and builds up in complexity with the next page. The search bar is also very good. I have yet to type something that didn’t come up with the page I was looking for. In summary, progressive, interactive and searchable documentation goes a long way to providing a great <code>“self-serve”</code> support experience.</p>

      <h3>Why do you think SSL/HTTPS is important?</h3>
      <p>Providing an encrypted and secure way to serve up content really makes a difference in the confidence of the website visitor as well as the peace of mind that no one can trivially inject unwanted content or tap into the TCP stream and ‘wiretap’ a visitor’s communication. But it is said that a chain is only as strong as the weakest link. As more and more websites are secured with SSL/TLS, it will become harder for malicious hackers to find a foothold for their nefarious deeds, and the stronger that weakest link would become. <code>”It’s dangerous to go alone. Here take this: HTTPS”</code></p>

      <h3>Explain, in a couple of paragraphs, what you think 2 major challenges around DNS configuration are for less-technical internet end-users</h3>
      <p>For less technical internet end-users, I think grasping the concept that the internet is addressed by a set of numbers arranged in 4 Octets (IPV4) and that the URL that user associates with the website is just a layer of abstraction on top. The fact that there are servers that direct “domain names” that people remember to the actual server that the site is on via its IP address might seem magical, as these users rarely, if ever, see an IP address. I imagine then telling the user that they can ‘move’ their entire site by simply changing the IP address that their DNS A record is pointing to. It would be pretty confusing without knowing at least the basics of networking.</p>
      <p>There is also the issue DNS propagation and TTL delay. With most things on the user-facing internet now being mostly ‘instant’, a less technical end-user could assume that making a change to their DNS configuration would yield instant results and when it doesn’t, they would think that either they made a mistake or something is broken. Resulting in confusion, frustration and likely a lot of swearing.</p>

      <h3>A customer writes in saying their “site won’t build”.  Compose your best short (2-paragraph) customer-facing answer without any additional data, that could be useful in the generic case but would also lead to a customer providing a more actionable response.</h3>
      <blockquote><i>
        <p>Hello there!</p>
        <blockquote>
          <p>Thanks for coming to us about the issue you were running into. There are a couple of things that can cause your site to fail to build. A couple of the most common causes are:</p>
          <li><strong>Case Sensitivity: </strong>
          On Windows and macOS, filenames aren’t case sensitive where jQuery is the same as jquery. However, Netlify servers would see those files as different.</li>
          <li><strong>Large Files: </strong>
          Files larger than 10 megabytes could cause a site to fail to build as these are not well supported by our CDN.</li>
          <p>There are a few other common reasons and we’ve listed them here: <a href="https://www.netlify.com/docs/build-gotchas">https://www.netlify.com/docs/build-gotchas</a>. If your site does continue to fail building, just let me know and we can go through the build process together to see why it’s failing and go from there.</p>
        </blockquote>
        <p>Thank you!<br />
          Dennis</p>
      </i></blockquote>

      <h3>(Optional/Bonus) Can you set up a redirect from “/netlify/anything” to <a href="https://www.google.com/search?q=anything">https://www.google.com/search?q=anything</a>?</h3>
      <p>Done! I love the way redirects are done with Netlify. Super simple.</p>
      <p><a href="/netlify/anything">A link using a _redirects file</a></p>
    </div>
  </div>
)

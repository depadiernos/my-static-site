import React from 'react'
import { getSiteProps, Link } from 'react-static'
//

export default getSiteProps(() => (
  <div>
    <h1 style={{ textAlign: 'center' }}>Welcome to</h1>
    <h2 style={{ textAlign: 'center' }}>My Netlify Homework</h2>
    <h3>Please look at the <Link to="/about">About</Link> page for my answers. :)</h3>

    <a href="/netlify/anything">A link using a _redirects file</a>
  </div>
))
